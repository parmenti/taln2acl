#!/usr/bin/env python3
from utils   import easychair, archives as arin, searchid
from exports import anthology, archives as arout, hal, dblp, pdf
import click, os, shutil, sys, subprocess, re

def print_help_msg(command):
    with click.Context(command) as ctx:
        click.echo(command.get_help(ctx))

@click.command()
@click.option('--indir', '-i', \
              help='''The directory where to find the event information,
              which consists in either yml/csv/html files or else an xml
              file together with pdf articles (see documentation)''')
@click.option('--outdir', '-o', default='build', \
              help='''The directory where to store the generated files 
              (default is build/)''')
@click.option('--in-format', default='easy', \
              help='''input format (options are easy [for easychair] (default) 
              or taln [for taln-archives])''',
              type=click.Choice(['easy', 'taln'], case_sensitive=False))
@click.option('--out-format', \
              help='''output format (options are "pdf", "acl" [for 
              www.aclweb.org/ACLanthology] (default), "taln" [for 
              talnarchives.atala.org], "hal" [for hal.archives-ouvertes.fr] 
              or dblp [for dblp.uni-trier.de]''',
              type=click.Choice(['pdf', 'acl', 'taln', 'hal', 'dblp'], \
                                case_sensitive=False))
@click.option('--xml', \
              help='''The name of the input XML file of the conference
              (TALN-archives input format)''')
@click.option('--img', \
              help='''The name of the directory where to found images
              needed for LaTeX compilation (e.g. sponsors' logos)''')
@click.option('--accept', default='ACCEPT', \
              help='Text used for decisions within easychair', \
              show_default=True)
@click.option('--anthology-id', \
              help='''The anthology ID of the conference''')
@click.option('--start', default=1, help='Page number of the first article', \
              show_default=True)
@click.option('--clean', is_flag=True, default=False, \
              help='Remove generated files.')
@click.option('--verbose', '-v', default=0, \
              help='Define verbosity between 0 and 3', show_default=True)
@click.option('--proceedings', '-p', default=False, is_flag=True, \
              help='Generate Anthology\'s PDF proceedings.')
@click.option('--bilingual', '-b', default=False, is_flag=True, \
              help='Generate bilingual article titles (cf anthology format).')
@click.option('--order', default='input', \
              help='order articles by author / title / input order (default is input)',
              type=click.Choice(['input', 'author', 'title'], case_sensitive=False))
@click.option('--stopwords', '-s', default=False, is_flag=True, \
              help='Ignore stopwords when sorting articles.')
@click.option('--ignore-pdf', default=False, is_flag=True, \
              help='Ignore metadata (abstract,keywords) from pdf ' + \
              'when these are available elsewhere', show_default=True)
@click.option('--dump', default=False, is_flag=True, \
              help='Keep a copy of easy2acl\'s configuration file.')
@click.option('--x-onbehalf', '-x', \
              help='''list of idHal_s (separated by ';') to be declared as co-owners 
              during HAL export (see https://aurehal.archives-ouvertes.fr/author)''')
@click.option('--stamp', \
              help='''Stamp (collection names, separated by ';') to be used during HAL
              export (see https://hal.archives-ouvertes.fr/browse/collection)''')
@click.option('--include-pdf', default=False, is_flag=True, \
              help='''During HAL export, generate full exports made of references + pdf 
              files.''')
@click.option('--guess', '-g', default=False, is_flag=True, \
              help='''During HAL export, activate guess affiliations from HAL records 
              or else from pdf files using grobid.''')  
@click.option('--dry-run', '-d', default=False, is_flag=True, \
              help='''During HAL export, use preprod server.''')  
@click.option('--halid', default=False, is_flag=True, \
              help='Include HAL id when compiling PDF proceedings.')
@click.option('--update', '-u', default=False, is_flag=True, \
              help='Update existing HAL reference/article.')
@click.option('--sessions', default=False, is_flag=True, \
              help='Do not ignore sessions in yaml configuration file.')
@click.option('--tex-log', default=False, is_flag=True, \
              help='Do not remove logs of LaTeX compilation.')
@click.option('--list-id', default=False, is_flag=True, \
              help='List hal identifiers (if any).', \
              show_default=True)
@click.option('--national', default=False, is_flag=True, \
              help='National event.', \
              show_default=True)
@click.option('--latex-encode', '-l', default=False, is_flag=True, \
              help='LaTeX-encode diacritics in output bibfiles.', \
              show_default=True)
@click.option('--no-meta', '-n', default=False, is_flag=True, \
              help='Do not update existing HAL reference/article metadata.')
@click.option('--tex-template', default='proceedings', \
              help='Tex template used for generating the PDF output (from tex/ dir)', \
              show_default=True)
def main(indir, outdir, in_format, out_format, xml, accept, anthology_id, start, \
         clean, verbose, proceedings, bilingual, stopwords, ignore_pdf, order, dump, \
         x_onbehalf, stamp, include_pdf, guess, dry_run, halid, update, sessions, \
         tex_log, list_id, no_meta, national, latex_encode, tex_template, img):
    ############################
    ## DECLARATION
    ############################
    e     = None #event
    where = None #source dir
    if os.path.exists(os.path.join(os.getcwd(), 'taln2acl.py')):
        # the script is being executed from its source directory
        where = os.getcwd()
    elif 'TALN2ACL_HOME' in os.environ:
        # otherwise
        where = os.environ['TALN2ACL_HOME']
    else:
        print('[Error] to invoke taln2acl.py from outside its source directory, ' + \
              'please define the TALN2ACL_HOME environment variable ' + \
              '(pointing to taln2acl\'s source dir) first', file=sys.stderr)
        sys.exit(1)
    outd  = outdir ##if outdir != 'build' else os.path.join(where, outdir) 
    ############################
    ## CLEANING
    ############################
    if not os.path.exists(outd):
        os.makedirs(outd)
    if clean:
        for the_file in os.listdir(outd):
            file_path = os.path.join(outd, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print(e, file=sys.stderr)
        print('Cleaning output directory ' + outd, file=sys.stderr)
        exit(0)
    elif in_format == 'taln':
        if not xml:
            print('\n[Error] Missing input XML file, exit.\n', file=sys.stderr)
            print_help_msg(main)
            exit(1)
    else:
        if not indir:
            print('\n[Error] Missing input repository, exit.\n', file=sys.stderr)
            print_help_msg(main)
            exit(1)
    ############################
    ## CASE: EASYCHAIR INPUT
    ############################
    if in_format == 'easy':
        if not os.path.exists(os.path.join(indir, 'event.yml')):
            print('\n[Error] Missing event yaml configuration file, exit.\n', \
                  file=sys.stderr)
            print_help_msg(main)
            sys.exit(12)
        print('\nReading event information\n=========================', file=sys.stderr)
        e = easychair.get_data(os.path.join(indir, 'event.yml'), accept, verbose, \
                           ignore_pdf, anthology_id, sessions)
        if verbose > 1:
            print(e)
    ############################
    ## CASE: TALN-ARCHIVES INPUT
    ############################
    elif in_format == 'taln':
        print('\nReading XML event file\n=========================', file=sys.stderr)
        try:
            e    = arin.read_xml(where, xml, anthology_id)
            indir= os.path.dirname(xml)
        except ValueError:
            print('[Error] Cannot read input XML file ' + xml + ', abort.', file=sys.stderr)
            exit(20)
        if verbose > 1:
            print(e)
    ############################
    ## CASE: SEARCH ID
    ############################
    if list_id:
        print('\nExtracting HAL identifiers\n=========================', file=sys.stderr)
        if shutil.which('curl') is None:
            print('[Error] You need to install \"curl\" to use the --list-id option.', \
                  file=sys.stderr)
            sys.exit(197)
        searchid.get_id(e, indir, order, stopwords, verbose, sessions)
    ############################
    ## CASE: PDF EXPORT
    ############################
    if out_format == 'pdf':
        if halid:
            if shutil.which('curl') is None:
                print('[Error] You need to install \"curl\" to use the --halid option.', \
                      file=sys.stderr)
                sys.exit(197)
        if not(os.path.exists(os.path.join(where, 'tex', tex_template+'.pre'))):
            print('[Error] Template not found in tex/ dir : ' + tex_template)
            sys.exit(210)
        print('\nGenerating PDF proceedings\n=========================', file=sys.stderr)
        pdf.write_tex(where, e, indir, outd, order, start, stopwords, verbose, halid, \
                      sessions, tex_log, tex_template, img)
    ############################
    ## CASE: EASY2ACL EXPORT
    ############################
    elif out_format == 'acl':
        print('\nGenerating ACL anthology files\n=========================', file=sys.stderr)
        anthology.easy2acl_export(where, e, order, indir, outd, proceedings, \
                                  start, bilingual, stopwords, verbose, dump, sessions, \
                                  latex_encode, tex_template, img)
    ############################
    # CASE: TALN ARCHIVES EXPORT
    ############################
    elif out_format == 'taln':
        print('\nGenerating event XML file\n=========================', file=sys.stderr)
        arout.write_xml(e, indir, outd, order, start, stopwords, verbose, dump, sessions, \
                        latex_encode)
    ############################
    # CASE: HAL ARCHIVES EXPORT
    ############################
    elif out_format == 'hal':
        print('\nGenerating event HAL BIB and ZIP files\n=========================', \
              file=sys.stderr)
        hal.write_hal_xml(where, e, indir, outd, order, start, stopwords, verbose, x_onbehalf,\
                          stamp, include_pdf, guess, dry_run, update, sessions, no_meta, national)
        print('\nSWORD shell scripts created, to use them, please export LOGIN and ' + \
              'PASSWORD environment variables containing your HAL credentials.\n', \
              file=sys.stderr)
    ############################
    # CASE: DBLP ARCHIVES EXPORT
    ############################
    elif out_format == 'dblp':
        print('\nGenerating event DBLP XML files\n=========================', file=sys.stderr)
        dblp.write_dblp_xml(where, e, indir, outd, order, start, stopwords, verbose, sessions)

if __name__ == '__main__':
    main()
