import os, sys, csv, shutil, subprocess, re, PyPDF2
import locale, threading
from contextlib  import contextmanager
from distutils   import dir_util
from unidecode   import unidecode
from .           import Event, Article, Author
from nameparser  import HumanName

# cf https://stackoverflow.com/questions/18593661/how-do-i-strftime-a-date-object-in-a-different-locale
LOCALE_LOCK = threading.Lock()

@contextmanager
def setlocale(name):
     with LOCALE_LOCK:
          saved = locale.setlocale(locale.LC_ALL)
          try:
               yield locale.setlocale(locale.LC_ALL, name)
          finally:
               locale.setlocale(locale.LC_ALL, saved)

def clean_easy2acl(where):
    tmpfiles = ['meta', 'submissions', 'submission.csv', 'accepted']
    tmpdir   = ['pdf', 'book-proceedings', 'proceedings']
    for f in tmpfiles: 
        try:
            os.remove(os.path.join(where, 'easy2acl',f))
        except FileNotFoundError as e:
            pass
    for d in tmpdir:
        try:
            shutil.rmtree(os.path.join(where, 'easy2acl',d))
        except FileNotFoundError as e:
            pass
    if not os.path.exists(os.path.join(where, 'easy2acl','book-proceedings')):
        os.mkdir(os.path.join(where, 'easy2acl','book-proceedings'))
    if not os.path.exists(os.path.join(where, 'easy2acl','pdf')):
        os.mkdir(os.path.join(where, 'easy2acl','pdf'))

def order_session(papers, real_order, stopwords, verbose, track):
    ordered = []
    for s in track['sessions'].split(';'):
        #collect corresponding papers
        spapers = list(filter(lambda x: x.__dict__['paperid'] in track['partition'][s.strip()], \
                              papers))
        ospapers= order_papers(spapers, real_order, stopwords, verbose, track['authors'])
        ordered.extend([None] + ospapers) #None is used as a session delimiter
    return ordered

def order_papers(papers, real_order, stopwords, verbose, tauthors):
    ordered = papers
    if real_order == 'author':
        try:
            pauthors = list(map(lambda x : [x.__dict__['paperid']] + \
                                list(map(lambda y : unidecode(y.lastname) + unidecode(y.firstname), \
                                         tauthors[int(x.__dict__['paperid'])])), \
                                papers))
            opauth   = sorted(pauthors, key=lambda z : z[1:]) #sort according to authors (not paperid)
            pdict   = {n.__dict__['paperid'] : n for n in papers}
            ordered  = list(map(lambda x : pdict[x[0]], opauth))
        except KeyError as ke:
            print('[Warning] Paper ' + str(ke) + ' not found in author_list.xlsx '+\
                  '\n(fallback on articles.csv for ordering authors).', file=sys.stderr)
            ordered = sorted(papers, key=lambda x : 
                             ''.join(list(map(lambda y : (unidecode(HumanName(y.strip()).middle)+\
                                                          unidecode(HumanName(y.strip()).last)+\
                                                          unidecode(HumanName(y.strip()).first)),\
                                              x.__dict__['authors'].replace(' and ', ', ')\
                                              .split(',')))))
        #to debug only:               
        #for x in ordered:
        #    print('*', x.__dict__['authors'])
    elif real_order == 'title':
        if stopwords:
            import nltk
            from nltk.corpus import stopwords as sw
            for x in papers:
                if x.__dict__['language'] == 'fr':
                    wordpos  = 0
                    sentence = re.split('\W+', x.__dict__['title'])
                    while wordpos in range(len(sentence)) \
                          and sentence[wordpos].lower() in \
                          [x for x in sw.words('french') if x not in ['nous']]:
                        # nous should not be considered a stopword
                        wordpos += 1
                    x.__dict__['titleTmp'] = unidecode((' '.join(sentence[wordpos:])).lower())
                else:
                    x.__dict__['titleTmp'] = unidecode(x.__dict__['title'].lower())
                if verbose > 2:
                    print(x.__dict__['title'])
                    print(x.__dict__['titleTmp'])
            ordered = sorted(papers, key=lambda x : x.__dict__['titleTmp'])
        else:
            ordered = sorted(papers, key=lambda x : unidecode(x.__dict__['title']))
    return ordered


def easy2acl_event(where, event, track):
    ACLINFO = ['anthology', 'title', 'month', 'year', \
               'location', 'publisher', 'chairs', 'shortbooktitle']
    # Turn e into easy2acl's expected config file
    with open(os.path.join(where, 'easy2acl','meta'), 'w') as outfile:
        for k,v in event.__dict__.items():
            if k in ACLINFO:
                if k == 'anthology':
                    print('abbrev' + ' ' + v.lower(), file=outfile)
                elif k == 'chairs': 
                    # if track has specific chairs, override general settings
                    if 'chairs' in event.__dict__['tracks'][track]:
                        for y in event.__dict__['tracks'][track]['chairs'].split(','):
                            print(k + ' ' + y.split()[1].strip() + ', ' + \
                                  y.split()[0].strip(), file=outfile)
                    else: #recall list of chairs are split over lines
                        for x in v:
                            print(k + ' ' + x.split(',')[0].strip() + ', ' + \
                                  x.split(',')[1].strip(), file=outfile)
                elif type(v) == list: 
                    for x in v:
                        print(str(k) + ' ' + str(v), file=outfile)
                else:
                    print(str(k) + ' ' + str(v), file=outfile)
        print('booktitle' + ' ' + event.__dict__['booktitle'] + '. ' +  \
              event.__dict__['tracks'][track]['fullname'], file=outfile)
        print('volume' + ' ' + event.__dict__['tracks'][track]['volume'], \
              file=outfile)
    if event.__dict__['anthology'] == '':
        print('Missing anthology id, cannot export to anthology format', file=sys.stderr)
        sys.exit(1)

def easy2acl_articles(where, e, papers, real_order, pdfdir, bilingual, stopwords, verbose, \
                      keep, sessions):
    # Start page (useful only in case of TALN import)
    taln_start= None
    # Ordered fields used in easy2acl
    SUBMISSIONS_PATTERN     = ['paperid', 'authors', 'title', 'url', 'keywords']
    SUBMISSIONS_CSV_PATTERN = ['paperid', 'title']
    trackname = os.path.basename(os.path.dirname(pdfdir))
    # Logging
    if keep:
        logfile = open(os.path.join(pdfdir, '..', 'submissions-'+ trackname +'.in.csv'),'w')
    with open(os.path.join(where, 'easy2acl','submissions'), 'w') as outfile1:
        with open(os.path.join(where, 'easy2acl','accepted'), 'w') as outfile2:
            with open(os.path.join(where, 'easy2acl','submission.csv'), 'w') as abs_csv:
                writer = csv.writer(abs_csv)
                writer.writerow(['#', 'abstract'])
                ordered = papers
                if sessions and 'sessions' in e.__dict__['tracks'][trackname]:
                    ordered = order_session(papers, real_order, stopwords, verbose, \
                                            e.__dict__['tracks'][trackname])
                else:
                    ordered = order_papers(papers, real_order, stopwords, verbose, \
                                           e.__dict__['tracks'][trackname]['authors'])
                for article in ordered:
                    if article:
                        #print('***',article.__dict__['title'])
                        if taln_start is None and 'pages' in article.__dict__ \
                           and article.__dict__['pages'] != '':
                            taln_start = article.__dict__['pages'].split('-')[0]
                        if bilingual:
                            article.update_title()
                        writer.writerow([article.__dict__['paperid'],\
                                         article.__dict__['abstract']])
                        #for submissions, only the first 3 columns are read by easy2acl:
                        print('\t'.join(article.__dict__[x] \
                                        if type(article.__dict__[x]) == str \
                                        else ', '.join(article.__dict__[x]) \
                                        for x in SUBMISSIONS_PATTERN), \
                              file=outfile1) 
                        #for accepted, we need the status
                        # print ID TITLE [...] STATUS => outfile2
                        print('\t'.join(article.__dict__[x] for x in SUBMISSIONS_CSV_PATTERN)+\
                              '\t'+'ACCEPT', file=outfile2)
                        #copy pdf file
                        if os.path.exists(os.path.join(pdfdir, article.__dict__['url'])):
                            if verbose > 1:
                                print('Copying pdf file ' + article.__dict__['url'], \
                                      file=sys.stderr)
                            shutil.copy(os.path.join(pdfdir, article.__dict__['url']), \
                                        os.path.join(where, 'easy2acl','pdf', \
                                                     e.__dict__['anthology'] + '_' + \
                                                     str(e.__dict__['year']) + '_paper_' + \
                                                     article.__dict__['paperid'] + '.pdf'))
                        #update log
                        if keep:
                            LOG_PATTERN=SUBMISSIONS_PATTERN[:3] +['track']+\
                                         [SUBMISSIONS_PATTERN[3]]+['title2','language','accept']+\
                                         [SUBMISSIONS_PATTERN[4]]
                            s = []
                            for y in LOG_PATTERN:
                                if y == 'url':
                                    s.append(os.path.basename(article.__dict__[y]))
                                elif y == 'accept':
                                    s.append('ACCEPT')
                                else:                            
                                    if type(article.__dict__[y]) == str :
                                        s.append(article.__dict__[y])
                                    else: 
                                        s.append(', '.join(article.__dict__[y]))
                            print('\t'.join(s),file=logfile) 
                    else: #session's delimiter
                        # we add an accepted article whose id is 0
                        # print ID TITLE [...] STATUS => outfile2
                        print('\t'.join(['-1', 'session delimiter'])+ \
                              '\t'+'ACCEPT', file=outfile2)
    #close log
    if keep:
        logfile.close()
    return taln_start

def easy2acl_bookproceedings(where, event, indir, track, start, tex_template, bilingual):
    source = indir
    dest   = os.path.join(where, 'easy2acl', 'book-proceedings')
    tex_pre= tex_template if tex_template is not None else 'book-proceedings.pre'
    if bilingual:
        locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
    ## Copy script files
    TEXSOURCES  =[tex_pre, 'by.eps', 'logo.png'] #(default logo, potentially overriden)
    EVENTSOURCES=['background.png', 'logo.png','sponsors.png', 'credits.tex']
    TRACKSOURCES=['message.tex', 'preface.tex', 'committee.tex', 'invited.tex']
    exists      = {}
    for x in TEXSOURCES:
        shutil.copy(os.path.join(where, 'tex', x), dest)
    for y in EVENTSOURCES: 
        if os.path.exists(os.path.join(source, y)):
            shutil.copy(os.path.join(source, y), dest)
            filename = y.split('.')[0]
            exists[filename] = True
    for z in TRACKSOURCES:
        if os.path.exists(os.path.join(source, track, z)):
            shutil.copy(os.path.join(source, track, z), dest)
            filename = z.split('.')[0]
            exists[filename] = True
        else:
            print('[Info] file ' + z + ' not found in ' + track, \
                  file=sys.stderr)
    ## Prepare book proceedings
    with open(os.path.join(where, 'easy2acl', 'book-proceedings', \
                           tex_pre), 'rt') as file_in:
        with open(os.path.join(where, 'easy2acl', 'book-proceedings', \
                               'book-proceedings.tex'), 'wt') as file_out:
            chairs = ', '.join(list(map(lambda x: x.split(', ')[1] + \
                                        ' ' + x.split(', ')[0], \
                                        event.__dict__['chairs'])))
            if 'chairs' in event.__dict__['tracks'][track].keys():
                chairs = event.__dict__['tracks'][track]['chairs']
            for line_in in file_in:
                # to replace: title - abbr - year - month - chairs - 
                # location - publisher - booktitle
                line_out = line_in 
                for check in ['background', 'logo', 'sponsors', 'credits', \
                              'message', 'preface', 'committee', 'invited']:
                    if check in exists.keys():
                        line_out = line_out.replace(check.upper()+'-PLACEHOLDER', \
                                                    '\\'+check+'true')
                    else:
                        line_out = line_out.replace(check.upper()+'-PLACEHOLDER', \
                                                    '\\'+check+'false')
                line_out = line_out.replace('PAGE-PLACEHOLDER',     \
                                            str(start))
                line_out = line_out.replace('TITLE-PLACEHOLDER',     \
                                            event.__dict__['title'])
                line_out = line_out.replace('ABBREV-PLACEHOLDER',    \
                                            event.__dict__['abbrev'])
                line_out = line_out.replace('SHORT-PLACEHOLDER',    \
                                            event.__dict__['shortbooktitle'])
                line_out = line_out.replace('YEAR-PLACEHOLDER',      \
                                            str(event.__dict__['year']))
                line_out = line_out.replace('URL-PLACEHOLDER',      \
                                            str(event.__dict__['url']))
                line_out = line_out.replace('MONTH-PLACEHOLDER',     \
                                            event.__dict__['begin'].strftime('%B'))
                line_out = line_out.replace('CHAIRS-PLACEHOLDER',     \
                                            chairs)
                line_out = line_out.replace('LOCATION-PLACEHOLDER',  \
                                            event.__dict__['location'])
                line_out = line_out.replace('PUBLISHER-PLACEHOLDER',  \
                                            event.__dict__['publisher'])
                line_out = line_out.replace('BOOK-PLACEHOLDER',      \
                                            event.__dict__['texbooktitle'] \
                                            if 'texbooktitle' in event.__dict__ \
                                            else event.__dict__['booktitle'])
                file_out.write(line_out)
    #Let us now compile the book proceedings
    curdir = os.getcwd()
    os.chdir(os.path.join(where, 'easy2acl', 'book-proceedings'))
    try:
        subprocess.call(['pdflatex','book-proceedings.tex'], \
                        stdout=subprocess.DEVNULL, timeout=20)
        subprocess.call(['pdflatex','book-proceedings.tex'], \
                        stdout=subprocess.DEVNULL, timeout=20)
        os.unlink('book-proceedings.log')
        os.unlink('book-proceedings.aux')
        os.unlink('book-proceedings.out')
        os.unlink('book-proceedings.toc')
        os.unlink('all_papers.aux')
        # let us extract the frontmatter 
        pdf       = PyPDF2.PdfFileReader('book-proceedings.pdf', strict=False)
        pdfwriter = PyPDF2.PdfFileWriter()
        # we look for the first occurrence of "Contents"
        numpages   = pdf.trailer['/Root']['/Pages']['/Count']
        i          = 0
        res_search = None
        #search pattern is the articles's footers without spaces:
        #Warning: it depends on tex/book-proceedings.pre
        pattern    = event.__dict__['title'].replace(' ','')# + \
                     #'\d+' + event.__dict__['publisher'].replace(' ','')
        #print("**pattern**", pattern)
        while i < numpages and res_search is None:
            PageObj = pdf.getPage(i)
            #print("this is page " + str(i)) 
            i += 1
            Text = PageObj.extractText() 
            #print(Text)
            res_search = re.search(pattern, Text)
            #print(res_search)
            if res_search is None:
                pdfwriter.addPage(PageObj)
        # we extract the corresponding pages
        with open(event.__dict__['anthology'] + '_' + str(event.__dict__['year']) + \
                  '_frontmatter.pdf', 'wb') as pdf_out:
            pdfwriter.write(pdf_out)
            pdf_out.close()
        # finally we copy the proceedings and frontmatter in the right place
        shutil.copy(event.__dict__['anthology'] + '_' + \
                    str(event.__dict__['year']) + '_frontmatter.pdf', \
                    os.path.join('..','pdf'))
        shutil.copy('book-proceedings.pdf', os.path.join('..','pdf', \
                                                         event.__dict__['anthology'] +'_'+ \
                                                         str(event.__dict__['year']) +'.pdf'))
    except subprocess.TimeoutExpired as toe:
        print('\n!!!! Compilation of book-proceedings.tex failed !!!!\n', file=sys.stderr)
    os.chdir(curdir)

def easy2acl_export(where, e, order, indir, outdir, proceedings, start, bilingual, stopwords, \
                    verbose, keep, sessions, latex_encode, tex_template, img):
    outdirname = os.path.join(outdir, 'acl_' + e.__dict__['abbrev'] + str(e.__dict__['year']))
    for track in e.__dict__['tracks'].keys():
        real_order = order
        real_start = start
        clean_easy2acl(where)
        print('Exporting event information', file=sys.stderr)
        easy2acl_event(where, e, track)
        #print(track, e.__dict__['tracks'][track].keys())
        print('Processing track ' + e.__dict__['tracks'][track]['fullname'], file=sys.stderr)
        print('Exporting articles information', file=sys.stderr)
        # track's config overrides command line one
        if 'order' in e.__dict__['tracks'][track].keys():
            real_order = e.__dict__['tracks'][track]['order']
        tstart = easy2acl_articles(where, e, e.__dict__['tracks'][track]['articles'], real_order, \
                                   os.path.join(indir, track, 'pdf'), bilingual, stopwords, \
                                   verbose, keep, sessions)
        # prepare tmp pdf file for frontmatter and full volume 
        # in case these won't be compiled by easy2acl itself 
        # e.g., in case --proceedings will not be used (default)
        if os.path.exists(os.path.join(indir, track, 'proceedings.pdf')):
            shutil.copy(os.path.join(indir, track, 'proceedings.pdf'), \
                        os.path.join(where, 'easy2acl', 'pdf', \
                                     e.__dict__['anthology'] + '_' + \
                                     str(e.__dict__['year']) + '.pdf'))
        else:
            shutil.copy(os.path.join(where, 'tex','blank.pdf'), \
                        os.path.join(where, 'easy2acl', 'pdf', \
                                     e.__dict__['anthology'] + '_' + \
                                     str(e.__dict__['year']) + '.pdf'))
        if os.path.exists(os.path.join(indir, track, 'frontmatter.pdf')):
            shutil.copy(os.path.join(indir, track, 'frontmatter.pdf'), \
                        os.path.join(where, 'easy2acl', 'pdf', \
                                     e.__dict__['anthology'] + '_' + \
                                     str(e.__dict__['year']) + \
                                     '_frontmatter.pdf'))
        else:
            shutil.copy(os.path.join(where, 'tex','blank.pdf'), \
                        os.path.join(where, 'easy2acl', 'pdf', \
                                     e.__dict__['anthology'] + '_' + \
                                     str(e.__dict__['year']) + \
                                     '_frontmatter.pdf'))
        # copy images if needed
        if img is not None:
            dir_util.copy_tree(img,\
                               os.path.join(where, 'easy2acl', 'book-proceedings'))
        print('Generating bibtex for articles using easy2acl', file=sys.stderr)
        # First call of easy2acl.py
        if tstart is not None: #read from input data (case of a TALN import)
            real_start = tstart
        curdir = os.getcwd()
        os.chdir(os.path.join(where, 'easy2acl'))
        # Track configuration overwrite cli start option
        if 'startpage' in e.__dict__['tracks'][track].keys():
            real_start = int(e.__dict__['tracks'][track]['startpage'])
        try:
            returncode = subprocess.call([os.path.join('.','easy2acl.py'), str(real_start), \
                                          str(latex_encode)], timeout=10) 
            ## Copy easy2acl's proceedings dir
            dirname = e.__dict__['abbrev'] + str(e.__dict__['year']) + \
                      '-' + track
            if os.path.exists(os.path.join(curdir, outdirname + '_data', dirname)):
                shutil.rmtree(os.path.join(curdir, outdirname + '_data', dirname))
            if returncode == 0:
                shutil.copytree('proceedings', \
                                os.path.join(curdir, outdirname + '_data', dirname, \
                                             'proceedings'))
            else:
                print('''\nCreation of proceedings in ACL format failed (call 1), 
                please check easy2acl's above message\n''', file=sys.stderr)
        except subprocess.TimeoutExpired as toe:
            print('''Creation of proceedings in ACL format failed (call 1), 
            please check the input files''', file=sys.stderr)
        os.chdir(curdir)

        if proceedings:
            # book proceedings pdf compilation
            print('Generating PDF proceedings', file=sys.stderr)
            easy2acl_bookproceedings(where, e, indir, track, real_start, tex_template, \
                                     bilingual)
            # Second call of easy2acl.py (to fix the frontmatter,
            # see https://github.com/acl-org/easy2acl)
            os.chdir(os.path.join(where, 'easy2acl'))
            try:
                returncode = subprocess.call([os.path.join('.','easy2acl.py'), \
                                              str(real_start)], \
                                             stdout=subprocess.DEVNULL, \
                                             stderr=subprocess.DEVNULL, \
                                             timeout=10)
                ## Copy easy2acl's proceedings dir
                dirname = e.__dict__['abbrev'] + str(e.__dict__['year']) + \
                          '-' + track
                if os.path.exists(os.path.join(curdir, outdirname + '_data', dirname)):
                    shutil.rmtree(os.path.join(curdir, outdirname + '_data', dirname))
                if returncode == 0:
                    shutil.copytree('proceedings', \
                                    os.path.join(curdir, outdirname + '_data', dirname, \
                                                 'proceedings'))
                else:
                    print('''Creation of proceedings in ACL format failed (call 2), 
                    please check easy2acl's above message''', file=sys.stderr)
            except subprocess.TimeoutExpired as toe:
                print('''Creation of proceedings in ACL format failed (call 2), 
                please check the latex compilation of book-proceedings.tex''',\
                      file=sys.stderr)
            os.chdir(curdir)
