import os, sys, re, shutil, subprocess, tqdm, PyPDF2, stat, json
from distutils             import dir_util
from urllib.parse          import quote
from exports.anthology     import order_papers, order_session
from exports.archives      import texify
from pylatexenc.latex2text import LatexNodes2Text

months_fr = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', \
             'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']

def clean_tex(f):
    if os.path.exists(f):
        os.unlink(f)

def write_tex(where, event, indir, outdir, order, start, stopwords, verbose, halid, \
              sessions, tex_log, tex_template, img):
    # Preparing output files
    eventname= event.__dict__['abbrev'] + \
               '-' + str(event.__dict__['year'])
    if not os.path.exists(os.path.join(outdir, 'pdf_' + eventname)):
        os.mkdir(os.path.join(outdir, 'pdf_' + eventname))
    ## Process articles
    for t in event.__dict__['tracks']:
        print('Processing track:', t, file=sys.stderr)
        source = indir
        dest   = os.path.join(outdir, 'pdf_' + eventname, t)
        if not os.path.exists(dest):
            os.mkdir(dest)
        if not os.path.exists(os.path.join(dest, 'pdf')):
            os.mkdir(os.path.join(dest, 'pdf'))
        if not os.path.exists(os.path.join(dest, 'pdf_out')):
            os.mkdir(os.path.join(dest, 'pdf_out'))
        allpapers = open(os.path.join(dest, 'all_papers.tex'), 'wt')
        ## Process paper in right order (cf automatic numbering)
        papers  = event.__dict__['tracks'][t]['articles']
        # track's config overrides command line one
        real_start = start
        if 'startpage' in event.__dict__['tracks'][t].keys():
            real_start = int(event.__dict__['tracks'][t]['startpage'])
        real_order = order
        if 'order' in event.__dict__['tracks'][t].keys():
            real_order = event.__dict__['tracks'][t]['order']
        ordered     = papers
        if sessions and 'sessions' in event.__dict__['tracks'][t]:
            ordered = order_session(papers, real_order, stopwords, verbose, \
                                    event.__dict__['tracks'][t])
        else:
            ordered = order_papers(papers, real_order, stopwords, verbose, \
                                   event.__dict__['tracks'][t]['authors'])
        for art in tqdm.tqdm(ordered):
            if art:
                process_article(event, t, indir, os.path.join(outdir, 'pdf_' + eventname), \
                                art, allpapers, halid, sessions)
            #no else since session's separator pages (i.e. when art is None) are taken 
            #care of by latex
        allpapers.close()
        ## Compiling PDF proceedings
        ## Copy files
        TEXSOURCES  =[ tex_template+'.pre', 'by.eps']
        EVENTSOURCES=['background.png', 'logo.png','sponsors.png', 'credits.tex']
        TRACKSOURCES=['message.tex', 'preface.tex', 'committee.tex', 'invited.tex', 'isbn.tex']
        exists      = {}
        for x in TEXSOURCES:
            shutil.copy(os.path.join(where, 'tex', x), dest)
        for y in EVENTSOURCES: 
            if os.path.exists(os.path.join(source, y)):
                shutil.copy(os.path.join(source, y), dest)
                filename = y.split('.')[0]
                exists[filename] = True
            else:
                print('[Info] file ' + y + ' not found', \
                      file=sys.stderr)
        for z in TRACKSOURCES:
            if os.path.exists(os.path.join(source, t, z)):
                shutil.copy(os.path.join(source, t, z), dest)
                filename = z.split('.')[0]
                exists[filename] = True
            else:
                print('[Info] file ' + z + ' not found in ' + t, \
                      file=sys.stderr)
        ## Copy users' images if needed
        if img is not None:
            dir_util.copy_tree(img, dest)
        ## Prepare proceedings
        with open(os.path.join(dest, tex_template+'.pre'), 'rt') as file_in:
            with open(os.path.join(dest, tex_template+'.tex'), 'wt') as file_out:
                chairs = ', '.join(list(map(lambda x: x.split(', ')[1] + \
                                            ' ' + x.split(', ')[0], \
                                            event.__dict__['chairs'])))
                if 'chairs' in event.__dict__['tracks'][t].keys():
                    chairs = event.__dict__['tracks'][t]['chairs']
                for line_in in file_in:
                    # to replace: title - abbr - year - month - chairs - 
                    # location - publisher - booktitle
                    line_out = line_in 
                    for check in ['background', 'logo', 'sponsors', 'credits', \
                                  'message', 'preface', 'committee', 'invited', 'isbn']:
                        if check in exists.keys():
                            line_out = line_out.replace(check.upper()+'-PLACEHOLDER', \
                                                        '\\'+check+'true')
                        else:
                            line_out = line_out.replace(check.upper()+'-PLACEHOLDER', \
                                                        '\\'+check+'false')
                    if halid:
                        line_out = line_out.replace('HAL-PLACEHOLDER', \
                                                    '\\HAL{\\halid}')
                    else:
                        line_out = line_out.replace('HAL-PLACEHOLDER', \
                                                    '')
                    if sessions:
                        line_out = line_out.replace('SESSION-PLACEHOLDER', \
                                                    '\\cursession,')
                    else:
                        line_out = line_out.replace('SESSION-PLACEHOLDER', \
                                                    '')

                    line_out = line_out.replace('TRACK-NAME-PLACEHOLDER', \
                                                event.__dict__['tracks'][t]['fullname'])
                    line_out = line_out.replace('PAGE-PLACEHOLDER',     \
                                                str(real_start))
                    line_out = line_out.replace('TITLE-PLACEHOLDER',     \
                                                event.__dict__['title'].\
                                                replace(' ; ','\\\\%\n'))
                    line_out = line_out.replace('ABBREV-PLACEHOLDER',    \
                                                event.__dict__['abbrev'])
                    line_out = line_out.replace('SHORT-PLACEHOLDER',    \
                                                event.__dict__['shortbooktitle'])
                    line_out = line_out.replace('YEAR-PLACEHOLDER',    \
                                                str(event.__dict__['year']))
                    begin = event.__dict__['begin'].strftime('%Y-%m-%d')
                    end   = event.__dict__['end'].strftime('%Y-%m-%d')
                    line_out = line_out.replace('DATE-PLACEHOLDER',      \
                                                '{\\origdate \\daterange{' + \
                                                begin + '}{' + end +'} }')
                    line_out = line_out.replace('URL-PLACEHOLDER',      \
                                                str(event.__dict__['url']))
                    line_out = line_out.replace('CHAIRS-PLACEHOLDER',     \
                                                chairs)
                    line_out = line_out.replace('LOCATION-PLACEHOLDER',  \
                                                event.__dict__['location'])
                    line_out = line_out.replace('PUBLISHER-PLACEHOLDER',  \
                                                event.__dict__['publisher'])
                    line_out = line_out.replace('BOOK-PLACEHOLDER',      \
                                                event.__dict__['texbooktitle'] \
                                                if 'texbooktitle' in event.__dict__ \
                                                else event.__dict__['booktitle'])
                    file_out.write(line_out)
        #Let us now compile the proceedings
        curdir = os.getcwd()
        try:
            os.chdir(dest)
            subprocess.call(['pdflatex', tex_template+'.tex'], \
                            stdout=subprocess.DEVNULL, 
                            stderr=subprocess.DEVNULL, \
                            timeout=20)
            subprocess.call(['pdflatex', tex_template+'.tex'], \
                            stdout=subprocess.DEVNULL, \
                            stderr=subprocess.DEVNULL, \
                            timeout=20)
            if not tex_log:
                clean_tex(tex_template+'.log')
                clean_tex(tex_template+'.aux')
                clean_tex(tex_template+'.out')
                clean_tex(tex_template+'.toc')
            clean_tex('all_papers.aux')
            os.chdir(curdir)
            # let us finally extract papers
            pdfreader = PyPDF2.PdfFileReader(os.path.join(outdir, 'pdf_'+eventname,\
                                                          t, tex_template+'.pdf'), \
                                             strict=False)
            # let us process frontmatter
            cpt       = 0
            res_search= None
            pnumpages = pdfreader.trailer['/Root']['/Pages']['/Count']
            frontprint= PyPDF2.PdfFileWriter() 
            while cpt < pnumpages and res_search is None:
                PageObj = pdfreader.getPage(cpt)
                cpt += 1
                Text = PageObj.extractText()
                #print(Text)
                res_search = re.search('Attribution4.0International', Text) #first paper found
                if res_search is None:
                    frontprint.addPage(PageObj)
            begin   = cpt - 1 if not (sessions and 'sessions' in event.__dict__['tracks'][t]) \
                      else cpt - 2
            frontout= open(os.path.join(outdir, 'pdf_' + eventname, t, 'frontmatter.pdf'), \
                           'wb')
            frontprint.write(frontout)
            frontout.close()
            #print(cpt)
            # let us then compute parsers' pages (to update their header/footer)
            print('Extracting pdfs of articles ...')
            for art in ordered:
                if art:
                    pdfartwriter = PyPDF2.PdfFileWriter()
                    end = begin + art.__dict__['numpages']
                    #print('**', begin, end)
                    for i in range(begin, end):
                        PageObject = pdfreader.getPage(i)
                        pdfartwriter.addPage(PageObject)
                        # we extract the corresponding pages
                        with open(os.path.join(outdir, 'pdf_' + eventname, t, 'pdf_out', \
                                               os.path.basename(art.__dict__['url'])), \
                                  'wb') as pdfart:
                            pdfartwriter.write(pdfart)
                            pdfart.close()
                        # we update counters
                        begin = end
                else: # we skip session's delimiter
                    begin += 1
        except subprocess.TimeoutExpired as toe:
            print('\n!!!! LaTeX compilation failed !!!!\n', file=sys.stderr)
            sys.exit(198)

def process_article(event, track, idir, odir, art, allpapersh, halid, sessions):
    origfile= os.path.join(idir, track, 'pdf', os.path.basename(art.__dict__['url']))
    outfile = os.path.join(odir, track, 'pdf', os.path.basename(art.__dict__['url']))
    shutil.copy(origfile, outfile)
    os.chmod(outfile, stat.S_IRUSR | stat.S_IWUSR) #set read/write rights
    ## Extract hyperlinks using pax/PDFBox
    if shutil.which('pdfannotextractor') is None:
        print('Error: pax tex package is missing ' + \
              '(https://www.ctan.org/tex-archive/macros/latex/contrib/pax), ' + \
              'in deb-based linux, it is part of texlive-latex-extra.', file=sys.stderr)
        sys.exit(199)
    else:
        os.environ["CLASSPATH"] = os.path.join('tex','PDFBox-0.7.3.jar')
        try:
            subprocess.call(['pdfannotextractor',outfile], \
                            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, timeout=20)
        except subprocess.TimeoutExpired as toe:
            print('Hyperlink extraction failed', file=sys.stderr)
    ## Update allpapers.tex
    hal = ''
    if halid:
        if art.__dict__['idhal']=='':
            out = subprocess.check_output(["curl", "-s", \
                                           'https://api.archives-ouvertes.fr/search/?q=title_s:\"'\
                               + quote(LatexNodes2Text().latex_to_text(art.__dict__['title'])).replace('%C2%A0', '%20')\
                                           .replace('%20%20%20', '%26') + '\"&fl=halId_s'])
            if int(json.loads(out.decode())['response']['numFound']) == 1:
                hal = json.loads(out.decode())['response']['docs'][0]['halId_s']
            elif int(json.loads(out.decode())['response']['numFound']) > 1:
                hal = json.loads(out.decode())['response']['docs'][0]['halId_s']
                print('[Warning] Several HAL identifiers found for paper ' + art.__dict__['paperid'] \
                      + '. Using ' + hal, \
                      file=sys.stderr)
            else:
                print('[Warning] HAL identifier not found for paper ' + art.__dict__['paperid'], \
                      file=sys.stderr)
        else:
            hal = art.__dict__['idhal']
            print('[Info] Using default id-hal from the csv file for paper ' + art.__dict__['paperid'] \
                  + ' (' + hal + ')', \
                  file=sys.stderr)
    session = ''
    if sessions and art.__dict__['session']:
        session = art.__dict__['session']
    s = '\\goodpaper{pdf/' + os.path.basename(art.__dict__['url']) + '}{' \
        + texify(art.__dict__['title']) + '}%\n{' + \
        texify(art.__dict__['authors'].replace(' and ', ', ')) + \
        '}{'+hal+'}{'+session+'}\n\n' 
    allpapersh.write(s)
