#!/bin/bash

## Facility script used to get a rough diff between 
## the PDF proceedings and automatically generated bibtex ones

actes=$1
bib="$2 $3 $4"

less $actes | grep -A 400 "matières" | grep -E "\.\s*[0-9]+$" | sed -e 's/ \.//g' | sed -e 's/   //' | sed -e 's/\([a-z]\)-\([A-Z]\)/\1 \2/' | unaccent "utf8" > /tmp/actes-tmp.txt

echo "\begin{document}" > /tmp/bib-tmp.txt

for i in "$(echo $bib)" ; do less $i | grep -E "(author|pages)" | cut -d= -f2 | tail -n +2 | tr '-' '\n' | sed -e 's/["}],/ /' | grep -v "^[0-9]" | grep -vE "^$" | sed -e 's/ "//g' | tr '\n' ' ' | sed -e 's/ and /, /g' | sed -e 's/\([0-9]\) /\1\n/g' | sed -e 's/\. / /g' | tr -s ' ' | sed -e 's/^ //' ; done >> /tmp/bib-tmp.txt

echo "\end{document}" >> /tmp/bib-tmp.txt

detex /tmp/bib-tmp.txt > /tmp/bib-tmp2.txt

diff /tmp/actes-tmp.txt /tmp/bib-tmp2.txt

