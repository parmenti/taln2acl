<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:template match="/">
    <html>
      <head>
        <title>DBLP Submission -- draft view</title>
      </head>
      <body>
        <xsl:apply-templates />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="proceedings">
    <h1>
      <xsl:if test="conf/number">
        <xsl:value-of select="conf/number" />
        <xsl:text>. </xsl:text>
      </xsl:if>
      <xsl:if test="conf/acronym">
        <xsl:value-of select="conf/acronym" />
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:value-of select="year" />
      <xsl:text>: </xsl:text>
      <xsl:value-of select="conf/location" />
    </h1>
    <p>
      <xsl:for-each select="editor">
        <a href=""><xsl:value-of select="." /></a>,
      </xsl:for-each>
      <xsl:text> </xsl:text>
      <xsl:value-of select="title" />
      <xsl:text> </xsl:text>
      <xsl:if test="conf/date">
        <xsl:value-of select="conf/date" />
      <xsl:text>. </xsl:text>
      </xsl:if>
      <xsl:if test="series/title">
        <xsl:value-of select="series/title" />
        <xsl:if test="series/number">
          <xsl:text> </xsl:text>
           <xsl:value-of select="series/number" />
        </xsl:if>
      <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:value-of select="publisher" />
      <xsl:text>, </xsl:text>
      <xsl:value-of select="year" />
      <xsl:text>, ISBN </xsl:text>
      <xsl:value-of select="isbn[1]" />
      <xsl:text>. </xsl:text>
      <xsl:apply-templates select="doi" />
    </p>
    <xsl:if test="conf/url">
      <p><xsl:value-of select="conf/url" /> [URL]</p>
    </xsl:if>
    <xsl:apply-templates select="toc" />
  </xsl:template>

  <xsl:template match="journal">
    <xsl:apply-templates select="volume" />
  </xsl:template>

  <xsl:template match="volume">
    <h1>
      <xsl:value-of select="../title" />
      <xsl:text>, Volume </xsl:text>
      <xsl:value-of select="number" />
    </h1>
    <xsl:apply-templates select="issue" />
  </xsl:template>

  <xsl:template match="issue">
    <h2>
      <xsl:text> Volume </xsl:text>
      <xsl:value-of select="../number" />
      <xsl:text>, Number </xsl:text>
      <xsl:value-of select="number" />
      <xsl:text>, </xsl:text>
      <xsl:value-of select="month" />
      <xsl:text> </xsl:text>
      <xsl:value-of select="year" />
    </h2>
    <xsl:apply-templates select="toc" />
  </xsl:template>

  <xsl:template match="toc">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="section">
    <xsl:choose>
    <xsl:when test="../../../../.[name()='journal']">
      <p><xsl:value-of select="." /></p>
    </xsl:when>
    <xsl:otherwise>
      <h3><xsl:value-of select="." /></h3>
    </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="subsection">
    <xsl:choose>
      <xsl:when test="../../../../.[name()='journal']">
        <p><xsl:value-of select="." /></p>
      </xsl:when>
      <xsl:otherwise>
        <h4><xsl:value-of select="." /></h4>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="subsubsection">
    <xsl:choose>
      <xsl:when test="../../../../.[name()='journal']">
        <p><xsl:value-of select="." /></p>
      </xsl:when>
      <xsl:otherwise>
        <h5><xsl:value-of select="." /></h5>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="sububsubsection">
    <xsl:choose>
      <xsl:when test="../../../../.[name()='journal']">
        <p><xsl:value-of select="." /></p>
      </xsl:when>
      <xsl:otherwise>
        <h6><xsl:value-of select="." /></h6>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="text">
    <p><xsl:value-of select="." /></p>
  </xsl:template>

  <xsl:template match="publ">
    <ul>
      <li>
        <xsl:for-each select="author">
          <a href=""><xsl:value-of select="." /></a>,
        </xsl:for-each>
        <xsl:if test="author"><br /></xsl:if>
        <b><xsl:value-of select="title" /></b>
        <xsl:text> </xsl:text>
        <xsl:value-of select="pages" />
        <xsl:choose>
          <xsl:when test="doi">
            <br />
            <xsl:apply-templates select="doi" />
            [electronic edition]
          </xsl:when>
           <xsl:otherwise>
             <xsl:if test="ee">
               <br />
               <xsl:value-of select="ee" />
               [electronic edition]
             </xsl:if>
           </xsl:otherwise>
        </xsl:choose>
      </li>
    </ul>
  </xsl:template>

  <xsl:template match="doi">
    <xsl:choose>
      <xsl:when test="@proxy">
        <xsl:value-of select="@proxy" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>http://dx.doi.org/</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="." />
  </xsl:template>

</xsl:stylesheet>
