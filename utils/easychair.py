import os, sys, yaml, csv, shutil, html, re, subprocess, PyPDF2
from openpyxl    import load_workbook
from unicodedata import normalize
from tqdm        import tqdm
from langdetect  import detect
from io          import StringIO
from bs4         import BeautifulSoup
from nameparser  import HumanName
from datetime    import datetime
from .           import Event, Article, Author

def get_meta(config, anthology_id):
    event = Event()
    doc = {}
    with open(config, 'r') as f:
        try:
            doc = yaml.safe_load(f)
        except yaml.YAMLError as exc:
            print(exc)
            pm = exc.problem_mark
            print("Your file {} has an issue on line {} at position {}".
                  format(pm.name, pm.line, pm.column))
        #print(doc)
    # Feed an Event object
    for k,v in doc.items():
        event.__dict__[k] = v
        if k == 'tracks':
            # Add volume name (for anthology)
            for x in v.keys():
                event.__dict__['tracks'][x]['volume'] = x
    # Command line flag anthology overrides config file's value
    if anthology_id is not None:
        event.__dict__['anthology'] = anthology_id
    # Auto-completions: (mandatory fields for anthology)
    if event.__dict__['shortbooktitle'] == '':
        if 'short_booktitle' in event.__dict__:
            event.__dict__['shortbooktitle'] = event.__dict__['short_booktitle']
        else:
            event.__dict__['shortbooktitle'] = event.__dict__['booktitle']
    if event.__dict__['begin'] is not None :
        event.__dict__['month'] = event.__dict__['begin'].month
        event.__dict__['year']  = event.__dict__['begin'].year
    # Convenience for LaTeX-PDF export (newlines in title)
    if '\\' in event.__dict__['booktitle']:
        event.__dict__['texbooktitle'] = event.__dict__['booktitle']
        event.__dict__['booktitle']    = event.__dict__['booktitle'].\
                                         replace('\\','')
    return event

def update_sessions(track, session, paper_id):
    if 'partition' not in track:
       track['partition'] = {} #dict mapping session id to paper ids
    if session not in track['partition']:
       track['partition'][session] = []
    track['partition'][session].append(paper_id)


def get_articles(papers, abstracts, indir, accept, track, \
                 verbose, ignore_pdf, sessions):
    '''
    This function processes articles of a given track.
    The accept parameter refers to the text to be used for acceptance 
    (default is ACCEPT).
    '''
    all_articles    = '' #real csv string computed from the input file
    all_papers      = [] #list of strings (rows of all_articles)
    title2id        = {} #mapping title   <-> paperid
    id2abstract     = {} #mapping paperid <-> abstract
    id2authors      = {} #mapping paperid <-> authors (cf incomplete csv files)
    # Fields in the submission page from easychair 
    # (unused ones are replaced by None):
    PAPERINFO       = ['paperid', 'authors', 'title', 'idhal', None, \
                       None, None, None, 'keywords']

    # [preprocessing] First, let us read the list of submitted papers
    # To fix keywords' format (newlines) to produce a proper csv string
    with open(papers, 'r') as f:
        current_article = None
        for line in f:
            line = normalize('NFC',line.strip())
            #print(line)
            if line[0].isdigit():
                if current_article is not None: 
                    all_articles += current_article + '\n'
                #we are moving to another article
                current_article = line
                #print(current_article)
                line_content = line.split('\t')
                #at the same time, we update the title2id dictionary
                #normalization wrt spaces
                #see https://stackoverflow.com/questions/2077897/
                #substitute-multiple-whitespace-with-single-whitespace-in-python
                norm_title = ' '.join(line_content[2].strip().lower().split())
                title2id[norm_title] = line_content[0].strip()
            else: #lines which do not start with a number are keywords to
                #be appended to a paper description
                current_article+= line
        # last entry should not be forgotten (since 
        # accumulation is triggered by new entries):
        all_articles += current_article 
        #print(all_articles)

    # Second, let us read the csv string to produce the files expected
    # by easy2acl (namely submissions and accepted) 

    # Substep 1: read the abstracts as well (beware it is an html file)
    if abstracts:
        with open(abstracts,'r') as f:
            content = f.read()
            soup = BeautifulSoup(content, 'html.parser')
            #print(soup.prettify())
            title    = ''
            abstract = ''
            for x in soup.find_all('div', attrs={'class':'paper'}):
                authors = x.find('span', attrs={'class': 'authors'}).get_text().strip()[:-1] 
                title   = x.find('span', attrs={'class': 'title'}).get_text().strip()
                abstract= x.findNext('div').get_text()[10:]
                try:
                    title_norm = ' '.join(title.lower().split())
                    title_norm = normalize('NFC', title_norm)
                    id2authors[title2id[title_norm]]  = authors
                    id2abstract[title2id[title_norm]] = abstract.replace('\n','')
                except KeyError as ke:
                    if verbose > 0 :
                        print('\n[Warning] Cannot find id for article entitled \"' + \
                              title + '\" (abstract in accepted.html ignored)', \
                              file=sys.stderr)
    # Substep 2: read the list of papers
    # and retrieve the corresponding pdf (e.g. for the English title)
    reader = csv.reader(StringIO(all_articles), delimiter='\t')
    for row in tqdm(reader, total=len(all_articles.split('\n'))):
        #print(PAPERINFO)
        #print(row)
        article = Article()
        patterns = accept.split(';')
        if any(list(map(lambda acc : re.match('.*'+acc.strip(), row[7], re.IGNORECASE), patterns))):
            try:
                article.__dict__['abstract'] = id2abstract[row[0]]
            except KeyError as e:
                pass # information already given in substep1
            ## update session info
            if sessions and ('sessions' in track):
                art_session = row[7].split('_')[1]
                if art_session in track['sessions'].split(';'):
                    article.__dict__['session'] = track[art_session]
                    update_sessions(track, art_session, row[0])
                else:
                    if verbose > 0:
                        print('[Warning] Unknown session: ' + \
                              art_session, file=sys.stderr)
            ## collect paper info
            for i in range(len(PAPERINFO)):
                if PAPERINFO[i] is not None:
                    try:
                        article.__dict__[PAPERINFO[i]] = row[i]
                    except IndexError as ie:
                        if verbose > 1:
                            print('Missing information in input csv file:', \
                                  PAPERINFO[i], file=sys.stderr) 
            midhal=re.fullmatch('hal-[0-9]*',article.__dict__['idhal'])
            if midhal is None:
                article.__dict__['idhal'] = ''
            if article.__dict__['authors'] == '': #missing info in articles.csv
                try: #get them from the abstracts' file
                    article.__dict__['authors'] = id2authors[article.__dict__['paperid']]
                except KeyError as ke:
                    if verbose > 0:
                        print('[Warning] no author found for article ' + \
                              str(article.__dict__['paperid']), file=sys.stderr)
            article.__dict__['url'] = os.path.join(os.getcwd(), indir, 'pdf', row[4])
            #print(article.__dict__['url'])

            ## Detect the article's language from its title
            article.__dict__['language'] = detect(row[2])
            #print('***', article.__dict__['title'], article.__dict__['language'])

            ## Extract the article's secondary title from the pdf (if possible)
            if not os.path.exists(os.path.join(indir, 'pdf', row[4])):
                print('[Error] PDF file not found: ' + os.path.join(indir, 'pdf', row[4]), \
                      file=sys.stderr)
                print('Please check input files', file=sys.stderr)
                sys.exit(1)
            pdf      = PyPDF2.PdfFileReader(os.path.join(indir, 'pdf', row[4]), \
                                            strict=False)
            numpages = pdf.trailer['/Root']['/Pages']['/Count']
            #print(article.__dict__['paperid'], numpages)
            article.__dict__['numpages'] = numpages
            try:
                if shutil.which('pdftotext') is None:
                    print('[Error] pdftotext is missing. ' + \
                          'Please install the xPdf command line tools,' + \
                          '(see http://www.xpdfreader.com/pdftotext-man.html).', \
                          file=sys.stderr)
                    sys.exit(199)
                args = ["pdftotext",
                        '-nopgbrk',
                        os.path.join(indir, 'pdf', row[4]),
                        '-']
                res = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                paper_content = res.stdout.decode('utf-8')
                #print('*',paper_content)
                m = None
                if article.__dict__['language'] == 'fr':
                    #Searching French Abstract:
                    mabs = re.search('[ÉE]\s*S\s*U\s*M\s*[ÉE][\_\.]*\n*(.+?)\n+' + \
                                     'A\s*B\s*S\s*T\s*R\s*A\s*C\s*T', \
                                     paper_content, re.DOTALL)
                    #Search French keywords:
                    mkey = re.search('M\s*O\s*T\s*S\s*-\s*C\s*L\s*[ÉE]\s*S\s*:\s*(.+?)\n+' + \
                                     'K\s*E\s*Y\s*', \
                                  paper_content, re.IGNORECASE|re.DOTALL)
                    #Searching English Title:
                    m = re.search('B\s*S\s*T\s*R\s*A\s*C\s*T\s*\_*\n+([^\.]+?)\.', \
                                  paper_content, re.DOTALL)
                else:
                    #Searching Englich Abstract:
                    mabs = re.search('A\s*B\s*S\s*T\s*R\s*A\s*C\s*T\s*[\_\.]*\n*(.+?)\n+' + \
                                     '(M\s*O\s*T\s*S|R\s*[ÉE]\s*S\s*U)', \
                                     paper_content, re.IGNORECASE|re.DOTALL)
                    #Search English keywords:
                    mkey = re.search('K\s*E\s*Y\s*W\s*O\s*R\s*D\s*S\s*:\s*(.+?)\n+' + \
                                     '1\s*', \
                                     paper_content, re.IGNORECASE|re.DOTALL)
                    #Searching French Title:
                    m = re.search('[ÉE]\s*S\s*U\s*M\s*[ÉE]\n+([^\.]+?)\.', \
                                  paper_content, re.IGNORECASE|re.DOTALL)
                if m: #secondary title found (right after RÉSUMÉ / ABSTRACT)
                    #in case the title is split over lines, we keep lines which
                    #do not start with an upper case letter (except when the preceding 
                    #line ends with more than 3 letters, e.g. it is not the determiner "A") 
                    title2 = re.sub(r'([\w-]{4,})(\s*[\?”"]?)\s*\n+[A-Z]', r'\1\2~', \
                                    m.group(1)).split('~')[0].replace('\n', ' ')
                    #print('\n*** before:', article.__dict__['paperid'], m.group(1), '***\n')
                    #print('\n*** found:', article.__dict__['paperid'], title2, '***\n')
                    article.__dict__['title2']    = title2
                    article.__dict__['language2'] = detect(title2)
                else: # for one paper, unicode was needed
                    m = re.search('A\n(.+?)\n', paper_content, re.IGNORECASE)
                    if m:
                        article.__dict__['title2']    = m.group(1)
                        article.__dict__['language2'] = detect(m.group(1))
                    else:
                        if verbose > 2:
                            print('[Warning] Secondary title extraction failed for paper ', \
                                  article.__dict__['title'])
                            #print(paper_content)
                if mabs and (not ignore_pdf or article.__dict__['abstract'] == ''):
                    article.__dict__['abstract'] = mabs.group(1).replace('\n', ' ')
                    #print('*',article.__dict__['abstract'])
                if mkey and (not ignore_pdf or article.__dict__['keywords'] == ''):
                    article.__dict__['keywords'] = mkey.group(1).replace('\n',' ')
                    #print('*',article.__dict__['keywords'])
            except UnicodeDecodeError as ud:
                if verbose >= 0:
                    print('[Warning] encoding error, ' + \
                          'paper ignored for pdf title/abstract extraction:', \
                          article.__dict__['title'], '('+article.__dict__['paperid']+')', \
                          file=sys.stderr)
                #English title read in column 4 in csv file (fallback)
                article.__dict__['title2'] = row[3]
            article.__dict__['track'] = track['volume']
            all_papers.append(article)
    return all_papers


def get_authors(authors):
    '''
    Functions which collects the authors of a given track
    '''
    all_authors      = {}
    previous_paperid = None
    author_counter   = 1
    # First, let us read the input XLSX file to turn it into some csv
    wb = load_workbook(authors, read_only=True)
    sh = wb.get_sheet_by_name('All')
    for row in sh.iter_rows(min_row=2):
        #print(row)
        if row[0].value is None:
            break
        values  = [cell.value for cell in row]  
        author  = Author()
        paperid = int(values[0])
        author.__dict__['paperid']     = str(paperid)
        if previous_paperid == paperid:
          author_counter += 1
        else:
          author_counter  = 1
        author.__dict__['rank']        = author_counter
        previous_paperid               = paperid
        if sh.max_column == 9: 
            #the excel file comes from a premium easychair account
            #the first and lastnames are in two columns
            fullname                       = HumanName(values[1] + ' ' + values[2])
            # "~" is used as an unsplittable space in first/last names
            author.__dict__['firstname']   = re.sub(r'([^\\])~', r'\1 ', fullname.first)
            author.__dict__['lastname']    = (fullname.middle + ' ' + fullname.last).strip()
            author.__dict__['email']       = values[3]
            author.__dict__['affiliation'] = html.unescape(values[5] + ', ' \
                                                           + values[4])              
        else:
            fullname                       = HumanName(values[1])
            # "~" is used as an unsplittable space in first/last names
            author.__dict__['firstname']   = re.sub(r'([^\\])~', r'\1 ', fullname.first)
            author.__dict__['lastname']    = (fullname.middle + ' ' + fullname.last).strip()
            author.__dict__['email']       = values[2]
            author.__dict__['affiliation'] = html.unescape(values[4] + ', ' \
                                                           + values[3])
        if paperid in all_authors.keys():
            all_authors[paperid].append(author)
        else:
            all_authors[paperid] = [author]
    return all_authors

def get_data(config, accept, verbose, ignore_pdf, anthology_id, sessions):
    e = get_meta(config, anthology_id)
    #print(e.__dict__['tracks'])
    for t in e.__dict__['tracks'].keys():
        print('Processing track:', t, file=sys.stderr)
        track_dir     = os.path.join(os.path.dirname(config), t)
        if not os.path.exists(track_dir):
            print(' [Warning] Directory missing for track ' + t + \
                  ' (track ignored).' , file=sys.stderr)
            #move on to the next track
        else: 
            papers_file   = os.path.join(track_dir, 'articles.csv')
            abstracts_file= os.path.join(track_dir, 'accepted.html')
            authors_file  = os.path.join(track_dir, 'author_list.xlsx')
            if not os.path.exists(os.path.join(track_dir, 'articles.csv')):
                print('\n[Error] Missing articles.csv for track ' + t + \
                      '\n(track ignored).\n', \
                      file=sys.stderr)
                continue
            if not os.path.exists(os.path.join(track_dir, 'accepted.html')):
                print('[Warning] accepted.html not found for track ' + t + \
                      '\n(abstracts will be extracted from pdf articles)', \
                      file=sys.stderr)
                abstracts_file = None
            if not os.path.exists(os.path.join(track_dir, 'author_list.xlsx')):
                print('[Warning] Missing author_list.xlsx for track ' + t + \
                      '\n(authors will be extracted from articles.csv and ' + \
                      'be missing affiliations)', file=sys.stderr)
                authors_file   = None
            #local accept pattern overrides command line one
            real_accept = accept #default case
            if 'accept' in e.__dict__['tracks'][t].keys():
                real_accept = e.__dict__['tracks'][t]['accept']
            articles      = get_articles(papers_file, abstracts_file, track_dir, \
                                         real_accept, e.__dict__['tracks'][t], \
                                         verbose, ignore_pdf, sessions)
            authors       = get_authors(authors_file) if authors_file else {}
            e.__dict__['tracks'][t]['articles'] = articles
            e.__dict__['tracks'][t]['authors']  = authors
    return e
